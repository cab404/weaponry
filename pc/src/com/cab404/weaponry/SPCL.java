package com.cab404.weaponry;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.cab404.defense.scene.SceneManager;
import com.cab404.guidy.Luna;
import com.cab404.weaponry.scene.ServerScene;
import com.cab404.weaponry.util.Init;

/**
 * @author cab404
 */
public class SPCL {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "Eponyr server";
        cfg.useGL20 = false;
        cfg.width = 50;
        cfg.height = 50;

        Init.run();
        Luna.scene = new SceneManager(new ServerScene());
        new LwjglApplication(Luna.scene, cfg);
    }
}
