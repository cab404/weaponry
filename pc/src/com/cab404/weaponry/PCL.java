package com.cab404.weaponry;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.cab404.defense.scene.SceneManager;
import com.cab404.guidy.Luna;
import com.cab404.weaponry.scene.Menu;
import com.cab404.weaponry.util.Init;

/**
 * PC Launcher
 *
 * @author cab404
 */
public class PCL {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "Eponyr";
        cfg.useGL20 = false;
        cfg.width = 800;
        cfg.height = 800;

        Init.run();
        Luna.scene = new SceneManager(new Menu());
        new LwjglApplication(Luna.scene, cfg);
    }

}
