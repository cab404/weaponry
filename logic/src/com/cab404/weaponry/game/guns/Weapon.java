package com.cab404.weaponry.game.guns;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.weaponry.game.GunGameObj;

import java.util.ArrayList;

/**
 * @author cab404
 */
public interface Weapon {
    public void shoot(GunGameObj src, ArrayList<GunGameObj> bullets, Vector2 vector);

    public int getAmmo();
    public int getMaxAmmo();
    public void addAmmo(int howMuch);

    public void switchedOn();
    public void switchedOff();

    public void update(float time);

    public String getName();

    public void render(GunGameObj src, SpriteBatch batch);

}
