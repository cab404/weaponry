package com.cab404.weaponry.game.guns;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.weaponry.game.GunGameObj;
import com.cab404.weaponry.game.Player;
import com.cab404.weaponry.util.SL;

import java.util.ArrayList;

/**
 * @author cab404
 */
public class GrenadeLauncher implements Weapon {
    public static class Grenade extends GunGameObj {
        public Grenade() {
            super();
            spr = SL.inst().load("data/imgs/weapons/bomb.png");
            size = new Vector2(32, 32);
            tileSize = new Vector2(8, 8);
            spr.setOrigin(size.x / 2, size.y / 2);
            setState(0);
        }

        float live = 0;
        @Override public void onHit(Side side, BoundingObject object) {
            super.onHit(side, object);
            if (!(object instanceof Player)) {
                if (side == Side.LEFT || side == Side.RIGHT) vel.x = -vel.x;
                else vel.y = -vel.y;
            } else {
                isAlive = false;
                float range = 300;
                float damage = 800;
                float vel = 50000;
                range *= range;

                for (GameObj obj : ((Player) object).parent.getAll()) {
                    if (obj instanceof GunGameObj) {
                        float dst = obj.pos.dst2(pos);
                        float div = dst / range;
                        if (dst < range) {
                            ((GunGameObj) obj).vel.add(obj.pos.cpy().add(obj.size.cpy().div(2)).sub(pos.cpy().add(size.cpy().div(2))).nor().mul(vel * div));
                            if (obj instanceof Player) {
                                ((Player) obj).hp -= div * damage;
                            }
                        }
                    }
                }
            }
        }

        @Override public void setState(int state) {
            if (state == 0) setTile(0, 0);
        }

        @Override public void update(float time) {
            super.update(time);

            spr.rotate(360 * time);
            live += time;
            if (live > 100) isAlive = false;
        }
    }

    float time;
    @Override public void shoot(GunGameObj src, ArrayList<GunGameObj> bullets, Vector2 vector) {
        if (time > 0.5) {
            time = 0;
            vector.rotate((float) (Math.random() - 0.5f) * 10);
            Grenade grenade = new Grenade();
            grenade.setBoundingObjects(src.getBoundingObjects());
            grenade.vel = vector.cpy().mul(1000);
            grenade.vel.add(src.vel);
            grenade.pos = src.pos.cpy().add(src.size.cpy().div(2)).add(vector.cpy().mul(80));
            bullets.add(grenade);
        }
    }

    @Override public int getAmmo() {
        return 0;
    }
    @Override public int getMaxAmmo() {
        return 0;
    }
    @Override public void addAmmo(int howMuch) {

    }
    @Override public void switchedOn() {

    }
    @Override public void switchedOff() {

    }

    @Override public void update(float time) {
        this.time += time;
    }

    @Override public String getName() {
        return "Grenade launcher";
    }
    @Override public void render(GunGameObj src, SpriteBatch batch) {

    }
}
