package com.cab404.weaponry.game.guns;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.weaponry.game.GunGameObj;
import com.cab404.weaponry.game.Player;
import com.cab404.weaponry.util.SL;

import java.util.ArrayList;

/**
 * @author cab404
 */
public class Uzi implements Weapon {

    float speed = 0.1f;

    public static class GunBullet extends GunGameObj {
        public GunBullet() {
            spr = SL.inst().load("data/imgs/weapons/bullet.png");
            tileSize = new Vector2(8, 8);
            size = new Vector2(16, 16);
        }

        @Override public void onHit(Side side, BoundingObject object) {
            super.onHit(side, object);
            if (object instanceof Player) {
                ((Player) object).hp -= 2;
            }
            isAlive = false;
        }

        @Override public void setState(int state) {
            if (state == 0) setTile(0, 0);
        }

        @Override public void render(SpriteBatch batch) {
            spr.setRotation(vel.angle());
            super.render(batch);

        }
    }

    @Override public void shoot(GunGameObj src, ArrayList<GunGameObj> bullets, Vector2 vector) {
        if (shoot > speed)
            if (getAmmo() > 0) {
                ammo--;
                shoot = 0;
                vector.rotate((float) (Math.random() - 0.5f) * 3);
                GunBullet gunBullet = new GunBullet();
                gunBullet.setBoundingObjects(src.getBoundingObjects());
                gunBullet.vel = vector.cpy().mul(1500).add(src.vel);
                gunBullet.setState(0);
                gunBullet.pos = src.pos.cpy().add(src.size.cpy().div(2)).add(vector.cpy().mul(80));
                bullets.add(gunBullet);
            }
    }

    int ammo = 120;
    @Override public int getAmmo() {
        return ammo;
    }
    @Override public int getMaxAmmo() {
        return 120;
    }

    @Override public void addAmmo(int howMuch) {
        ammo += howMuch * 50;
        if (ammo > getMaxAmmo()) ammo = getMaxAmmo();
    }

    @Override public void switchedOn() {

    }
    @Override public void switchedOff() {

    }

    float add_decay = 0;
    float shoot = 0;
    @Override public void update(float time) {
        add_decay += time;
        shoot += time;
        if (add_decay > 1) {
            ammo += 5;
            add_decay = 0;
        }
        if (ammo > getMaxAmmo()) ammo = getMaxAmmo();
    }

    @Override public String getName() {
        return "Uzi";
    }
    @Override public void render(GunGameObj src, SpriteBatch batch) {

    }
}
