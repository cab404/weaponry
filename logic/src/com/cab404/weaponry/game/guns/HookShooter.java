package com.cab404.weaponry.game.guns;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.weaponry.game.GunGameObj;
import com.cab404.weaponry.util.SL;

import java.util.ArrayList;

/**
 * @author cab404
 */
public class HookShooter implements Weapon {
    public float rope_length = 700;
    public float pull = 1500;


    public static class Hook extends GunGameObj {
        BoundingObject connected;
        GunGameObj src;
        HookShooter assoc = null;
        Vector2 offset;
        {G = 0;}

        public Hook() {
            super();
            spr = SL.inst().load("data/imgs/weapons/rope.png");
        }
        @Override public void onHit(Side side, BoundingObject obj) {
            if (connected == null) {
                if (src == obj) isAlive = false;
                connected = obj;
                offset = new Vector2(obj.pos.cpy().sub(pos));
            }
        }


        @Override public void setState(int state) {
            if (state == 0) setTile(0, 0);
            if (state == 1) setTile(0, 1);
        }

        @Override public void update(float time) {
            super.update(time);

            if (assoc == null) return;

            Vector2 source = src.pos.cpy().add(src.size.cpy().div(2));
            Vector2 posit = pos.cpy().add(size.cpy().div(2));

            if (connected != null) {
                pos.set(connected.pos.cpy().sub(offset));
                vel.mul(0);
                src.vel.add(posit.cpy().sub(source).nor().mul(assoc.pull * time));
                if (connected instanceof GunGameObj) {
                    ((GunGameObj) connected).vel.add(posit.cpy().sub(source).nor().mul(-assoc.pull * time));
                }

            } else if (pos.dst2(src.pos) > assoc.rope_length * assoc.rope_length) isAlive = false;


            spr.setRotation(source.sub(posit).angle() - 90);
            if (!assoc.shootLastTime) isAlive = false;
            assoc.shootLastTime = false;
        }

        @Override public void render(SpriteBatch batch) {
            setState(1);
            super.render(batch);
            if (assoc == null) return;
            Vector2 source = src.pos.cpy().add(src.size.cpy().div(2));
            Vector2 posit = pos.cpy().add(size.cpy().div(2));
            Vector2 dir = new Vector2(source).sub(posit).nor().mul(16);


            setState(0);
            for (int i = 0; i < Math.ceil(posit.dst(source) / 16 - 4); i++) {
                spr.translate(dir.x, dir.y);
                spr.draw(batch);
            }


        }
    }

    Hook hook;
    boolean shootLastTime = false;
    @Override public void shoot(GunGameObj src, ArrayList<GunGameObj> bullets, Vector2 vector) {
        shootLastTime = true;
        if (hook == null || !hook.isAlive) {
            hook = new Hook();
            hook.assoc = this;

            hook.vel = vector.cpy().mul(3000).add(src.vel);
            hook.src = src;
            hook.size = new Vector2(16, 16);
            hook.tileSize = new Vector2(8, 8);
            hook.pos.set(src.pos.cpy().add(src.size.cpy().div(2)).add(vector.cpy().mul(90)));

            hook.setBoundingObjects(src.getBoundingObjects());
            hook.setState(0);

            bullets.add(hook);
        }
    }

    @Override public int getAmmo() {
        return hook == null || !hook.isAlive ? 1 : 0;
    }
    @Override public int getMaxAmmo() {
        return 1;
    }
    @Override public void addAmmo(int howMuch) {}

    @Override public void switchedOn() {}

    @Override public void switchedOff() {}
    @Override public void update(float time) {

    }

    @Override public String getName() {
        return "Hook";
    }

    @Override public void render(GunGameObj src, SpriteBatch batch) {

    }
}
