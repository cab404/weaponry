package com.cab404.weaponry.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.weaponry.game.guns.Weapon;
import com.cab404.weaponry.util.SL;

/**
 * @author cab404
 */
public class Player extends GunGameObj {
    public Selector<Weapon> weaponry;
    {hp = 150;}
    public static final int
            IDLE = 0;


    public Player(Sprite spr) {
        size = new Vector2(64, 64);
        weaponry = new Selector<>();
        this.spr = spr;
        setTile(0, 0);
    }

    @Override public void update(float time) {
        if (hp < 1)
            isAlive = false;
        super.update(time);
    }

    public Player() {
        this.spr = SL.inst().load("data/imgs/chars/cab.png");
        size = new Vector2(64, 64);
        tileSize.set(16, 16);
        setTile(0, 0);
        weaponry = new Selector<>();
    }


    @Override public void onHit(Side side, BoundingObject object) {
        super.onHit(side, object);
    }

    @Override public char getState() {
        return (char) (
                super.getState()
                        + (isFlipped ? 32768 : 0)
        );
    }
    @Override public void setState(int state) {
        if (state >= 32768) {
            isFlipped = true;
            state -= 32768;
        } else {
            isFlipped = false;
        }

    }
}
