package com.cab404.weaponry.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cab404.defense.ui.ListMenu;
import com.cab404.defense.ui.TextField;
import com.cab404.weaponry.game.guns.HookShooter;
import com.cab404.weaponry.util.LOg;

/**
 * @author cab404
 */
public class MainPlayerController implements Controller<GunGameObj> {

    ListMenu weapons;
    HookShooter hook;


    public MainPlayerController() {
        weapons = new ListMenu();
        weapons.animationSpeed = Float.MIN_VALUE;
    }

    public void updateWeapons(final Player player) {
        LOg.vbs(player.hp);
        weapons.killall();
        weapons.add(new TextField() {
            @Override public String getText() {
                return "HP: " + player.hp;
            }
        }
        );
    }

    @Override public void render(GunGameObj object, SpriteBatch batch) {

        if (!(object instanceof Player))
            throw new RuntimeException("Non-Player class: " + object.getClass());

        weapons.pos.set(object.pos).add(object.size).add(20, 20);
        weapons.render(batch);

        if (((Player) object).weaponry.get() != null) {
            ((Player) object).weaponry.get().render(object, batch);
        }
    }


    @Override public void update(GunGameObj object, float time) {
//
        if (!(object instanceof Player))
            throw new RuntimeException("Non-Player class: " + object.getClass());
//
        weapons.update(time);
        Player obj = (Player) object;
//
//        if (Luna.key(Input.Keys.A)) {
//            if (obj.isOnGround())
//                obj.vel.x -= 2000 * time;
//            else
//                obj.vel.x -= 100 * time;
//        }
//
//        if (Luna.key(Input.Keys.D)) {
//            if (obj.isOnGround())
//                obj.vel.x += 2000 * time;
//            else
//                obj.vel.x += 100 * time;
//        }
//
//        if (Luna.key(Input.Keys.W) && obj.isOnGround()) {
//            obj.vel.y = 15000 * time;
//        }
//
//        if (obj.weaponry.get() != null)
//            obj.weaponry.get().update(time);
//
//        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
//            ArrayList<GunGameObj> bullets = new ArrayList<>();
//            if (obj.weaponry.get() != null)
//                obj.weaponry.get().shoot(obj, bullets, Luna.mouse(0).sub(Luna.screen().div(2)).div(2).nor());
//            Collections.addAll(U.aTGame, bullets.toArray(new GunGameObj[bullets.size()]));
////            obj.getBoundingObjects().addAll(bullets);
//        }
//        if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
//            ArrayList<GunGameObj> bullets = new ArrayList<>();
//            hook.shoot(obj, bullets, Luna.mouse(0).sub(Luna.screen().div(2)).div(2).nor());
//            Collections.addAll(U.aTGame, bullets.toArray(new GunGameObj[bullets.size()]));
//
//        }
//
//        obj.setState(Player.IDLE);
//
//        Vector2 mouse = Luna.mouse(0);
//        mouse.sub(Luna.screen().div(2));
//
//        if (mouse.x < 0) obj.isFlipped = false;
//        if (mouse.x > 0) obj.isFlipped = true;
//
//        if (U.getScroll() > 0 || Luna.jkey(Input.Keys.E)) {
//            obj.weaponry.next();
//            updateWeapons(obj);
//        }
//        if (U.getScroll() < 0 || Luna.jkey(Input.Keys.Q)) {
//            obj.weaponry.prev();
        updateWeapons(obj);
//        }

    }
}

