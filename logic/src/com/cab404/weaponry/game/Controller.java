package com.cab404.weaponry.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * @author cab404
 */
public interface Controller<T> {
    public void render(T object, SpriteBatch batch);
    public void update(T object, float time);
}
