package com.cab404.weaponry.game.assist;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;

/**
 * @author cab404
 */
public class ImageObj extends GameObj {
    public Sprite spr;

    public ImageObj(Sprite spr) {this.spr = spr;}

    public ImageObj setPos(Vector2 pos) {
        spr.setPosition(pos.x, pos.y);
        return this;
    }

    public ImageObj setSize(Vector2 size) {
        spr.setSize(size.x, size.y);
        return this;
    }

    @Override public void render(SpriteBatch batch) {
        spr.draw(batch);
    }

    @Override public void update(float time) {

    }
}
