package com.cab404.weaponry.game.assist;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.ui.Field;

/**
 * @author cab404
 */
public class ImageField implements Field {
    public Sprite spr;

    public ImageField() {}
    public ImageField(Sprite spr) {this.spr = spr;}

    @Override public void render(SpriteBatch batch, float phase, Vector2 lbc) {
        spr.setPosition(lbc.x, lbc.y);
        spr.setScale(phase, phase);
        spr.draw(batch);
    }

    @Override public Vector2 getSize(float phase) {
        return new Vector2(spr.getWidth() * phase, spr.getHeight() * phase);
    }

    @Override public void onMouseOver(Vector2 mouse_pos) {
    }

    @Override public void update(float time) {

    }
    @Override public boolean isDead() {
        return false;
    }
}
