package com.cab404.weaponry.game.assist;

import com.badlogic.gdx.InputProcessor;
import com.cab404.defense.objects.GameObj;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author cab404
 */
public class U {

    public static ArrayList<GameObj>
            aTPartc = new ArrayList<>(),
            aTGame = new ArrayList<>(),
            aTObj = new ArrayList<>();



    public static interface Processor<T> {
        public void process(T a, T b);
    }

    /**
     * Проходится по всем уникальным парам в массиве <b>objects</b>, запусткая на каждой паре <b>proc</b>
     */
    public static <T> void processAllObjects(Processor<T> proc, List<T> objects) {
        for (int i = 0; i < objects.size() - 1; i++) {
            Iterator<T> iter = objects.listIterator(i + 1);
            while (iter.hasNext()) {
                proc.process(objects.get(i), iter.next());
            }
        }
    }

    private static int scroll;
    public static int getScroll() {
        int ts = scroll;
        scroll = 0;
        return ts;
    }

    public static class PipeInputProcessor implements InputProcessor {
        @Override public boolean keyDown(int keycode) {
            return false;
        }
        @Override public boolean keyUp(int keycode) {
            return false;
        }
        @Override public boolean keyTyped(char character) {
            return false;
        }
        @Override public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            return false;
        }
        @Override public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            return false;
        }
        @Override public boolean touchDragged(int screenX, int screenY, int pointer) {
            return false;
        }
        @Override public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }
        @Override public boolean scrolled(int amount) {
            scroll = amount;
            return true;
        }
    }
}
