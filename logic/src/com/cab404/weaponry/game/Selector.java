package com.cab404.weaponry.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author cab404
 */
public class Selector<T> implements Iterable<T> {
    public List<T> list;
    private int selected_id;

    public Selector() {
        list = new ArrayList<>();
    }

    public void next() {
        selected_id++;
        check();
    }

    public void prev() {
        selected_id--;
        check();
    }

    public T get() {
        check();
        if (list.size() > 0)
            return list.get(selected_id);
        else
            return null;
    }

    private void check() {
        if (list.size() > 0)
            selected_id =
                    selected_id < 0
                            ? lastIndex()
                            : (
                            selected_id > lastIndex()
                                    ? 0
                                    : selected_id
                    );
    }


    private int lastIndex() {
        return list.size() - 1;
    }

    @Override public Iterator<T> iterator() {
        return list.iterator();
    }
}
