package com.cab404.weaponry.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.guidy.tests.platformer.PlatformerObject;

import java.util.ArrayList;

/**
 * @author cab404
 */
public abstract class GunGameObj extends PlatformerObject {
    protected Controller<GunGameObj> controller;

    public int oID;
    public int hp;
    {
        G = 186f;
        GF = 0.8f;
        AF = 0.5f;
    }

    public boolean isFlipped = false;
    public Vector2 tileSize = new Vector2(16, 16);
    public Sprite spr;

    private char state;
    public abstract void setState(int state);

    public char getState() {
        return state;
    }

    public GunGameObj() {
        setBoundingObjects(new ArrayList<BoundingObject>());
    }

    @Override public void update(float time) {
        super.update(time);
        if (controller != null)
            controller.update(this, time);
    }

    public void setTile(int x, int y) {
        spr.setRegion(x * (int) tileSize.x, y * (int) tileSize.y, (int) tileSize.x, (int) tileSize.y);
        spr.setSize(size.x, size.y);
    }

    public void setController(Controller<GunGameObj> controller) {
        if (controller != null)
            this.controller = controller;
    }

    public boolean hasController() {
        return controller != null;
    }

    @Override public void render(SpriteBatch batch) {
        if (controller != null)
            controller.render(this, batch);
        spr.setPosition(pos.x, pos.y);
        spr.flip(spr.isFlipX() ^ isFlipped, false);
        spr.draw(batch);
    }


}
