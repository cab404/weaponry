package com.cab404.weaponry.game.network;

/**
 * @author cab404
 */
public interface ServerPacketHandler {
    public boolean handle(Packet packet, int client_id);
}
