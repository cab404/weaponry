package com.cab404.weaponry.game.network;

import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cab404
 */
public class PacketTypeRegistry {

    private static PacketTypeRegistry singleton;

    public static PacketTypeRegistry getInstance() {
        if (singleton == null)
            singleton = new PacketTypeRegistry();
        return singleton;
    }

    private PacketTypeRegistry() {
        types = new HashMap<>();
    }

    public void register(Class<? extends Packet> packet) {
        try {
            types.put((Byte) packet.getField("PID").get(null), packet);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException("No byte PID field found in Packet class!");
        }
    }

    private Map<Byte, Class<? extends Packet>> types;

    public Packet getPacketForData(ByteBuffer data) {
        byte id = data.get(0);
        if (types.containsKey(id))
            try {
                return (Packet) types
                        .get(id)
                        .getMethod("deserialize", ByteBuffer.class)
                        .invoke(null, data);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                throw new RuntimeException("Non-supported handler in registry!", e);
            }
        else throw new RuntimeException("No handler found for packet type " + id);

    }
}
