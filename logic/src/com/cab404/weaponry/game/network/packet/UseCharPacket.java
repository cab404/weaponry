package com.cab404.weaponry.game.network.packet;

import com.cab404.weaponry.game.network.Packet;

import java.nio.ByteBuffer;

/**
 * @author cab404
 */
public class UseCharPacket implements Packet {
    public static final byte PID = 4;

    public int oID;                      // Object ID

    public UseCharPacket(int oID) {
        this.oID = oID;
    }


    @SuppressWarnings("UnusedDeclaration")
    public static UseCharPacket deserialize(ByteBuffer buf) {
        return new UseCharPacket(buf.getInt(1));
    }

    @Override
    public void serialize(ByteBuffer buffer) {
        buffer.put(0, PID);
        buffer.putInt(1, oID);
    }
}
