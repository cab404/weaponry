package com.cab404.weaponry.game.network.packet;

import com.cab404.weaponry.game.network.Packet;

import java.nio.ByteBuffer;

/**
 * @author cab404
 */
public class DelPacket implements Packet {
    public static final byte PID = 1;

    public int oID;                      // Object ID

    @SuppressWarnings("UnusedDeclaration")
    public static DelPacket deserialize(ByteBuffer buf) {
        DelPacket packet = new DelPacket();

        packet.oID = buf.getInt(1);
        return packet;
    }

    @Override
    public void serialize(ByteBuffer buffer) {
        buffer.put(0, PID);
        buffer.putInt(1, oID);
    }
}
