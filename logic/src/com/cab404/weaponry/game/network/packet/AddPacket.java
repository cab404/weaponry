package com.cab404.weaponry.game.network.packet;

import com.cab404.weaponry.game.network.Packet;

import java.nio.ByteBuffer;

/**
 * @author cab404
 */
public class AddPacket implements Packet {
    public static final byte PID = 0;

    public int hp;
    public int oID;                      // Object ID
    public char cID, sID;               // Class ID and State ID
    public float x, y, vx, vy;         // Actual data

    @SuppressWarnings("UnusedDeclaration")
    public static AddPacket deserialize(ByteBuffer buf) {
        AddPacket packet = new AddPacket();

        packet.oID = buf.getInt(1);
        packet.cID = buf.getChar(5);
        packet.sID = buf.getChar(7);
        packet.x = buf.getFloat(9);
        packet.y = buf.getFloat(13);
        packet.vx = buf.getFloat(17);
        packet.vy = buf.getFloat(21);
        packet.hp = buf.getInt(25);

        return packet;
    }

    @Override
    public void serialize(ByteBuffer buffer) {
        buffer.put(0, PID);
        buffer.putInt(1, oID);
        buffer.putChar(5, cID);
        buffer.putChar(7, sID);
        buffer.putFloat(9, x);
        buffer.putFloat(13, y);
        buffer.putFloat(17, vx);
        buffer.putFloat(21, vy);
        buffer.putInt(25, hp);

    }


}
