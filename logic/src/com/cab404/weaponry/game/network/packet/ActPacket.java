package com.cab404.weaponry.game.network.packet;

import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.Luna;
import com.cab404.weaponry.game.network.Packet;

import java.nio.ByteBuffer;

/**
 * @author cab404
 */
public class ActPacket implements Packet {
    public static final byte PID = 3;

    public int aID;                      // Action ID

    public Vector2 mouse;

    public ActPacket() {
    }


    public ActPacket(int aID) {
        this.aID = aID;
    }


    @SuppressWarnings("UnusedDeclaration")
    public static ActPacket deserialize(ByteBuffer buf) {
        ActPacket packet = new ActPacket();

        packet.aID = buf.getInt(1);
        packet.mouse = new Vector2(buf.getFloat(5), buf.getFloat(9));

        return packet;
    }

    @Override
    public void serialize(ByteBuffer buffer) {
        buffer.put(0, PID);
        buffer.putInt(1, aID);

        Vector2 mouse = Luna.mouse(0);
        mouse.sub(Luna.screen().div(2));

        buffer.putFloat(5, mouse.x);
        buffer.putFloat(9, mouse.y);
    }


}
