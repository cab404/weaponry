package com.cab404.weaponry.game.network;

import java.util.List;

/**
 * @author cab404
 */
public interface ServerPacketProvider {
    public List<Packet> getPackets(int client_id);
    public List<Packet> getBroadcastPackets();
}
