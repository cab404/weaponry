package com.cab404.weaponry.game.network;

import java.nio.ByteBuffer;

/**
 * <html>
 * Should declare static method:
 * </br>
 * public static Packet deserialize(ByteBuffer buffer);
 * </html>
 *
 * @author cab404
 */
public interface Packet {
    public void serialize(ByteBuffer buffer);
}
