package com.cab404.weaponry.game.network;

import com.cab404.guidy.Luna;
import com.cab404.weaponry.game.network.server.*;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author cab404
 */
public class SL {

    public static void main(String[] args) {
        final CopyOnWriteArraySet<Integer> ids = new CopyOnWriteArraySet<>();

        try {
            ServerConfig params = new ServerConfig();
            params.port = 8080;
            final Server server = new Server(params);

            server.setConnectionGainListener(new ConnectionListener() {
                @Override public void process(Server s, InetAddress address, int client_id) {
                    System.out.println("Connection gain. id " + client_id + "/" + s.getConnectionNum() + ", threads: " + s.getThreadNum());
                    ids.add(client_id);
                }
            });

            server.setConnectionLostListener(new ConnectionListener() {
                @Override public void process(Server s, InetAddress address, int client_id) {
                    System.out.println("Connection lost. id " + client_id + "/" + s.getConnectionNum() + ", threads: " + s.getThreadNum());
                    ids.remove(client_id);
                }
            });

            server.setInputListener(new InputListener() {
                @Override public void retrieveData(Server s, ByteBuffer buf, int client_id) {
                    Luna.log(Arrays.toString(buf.array()));
                }
            });

            server.setOutputListener(new OutputListener() {
                @Override public boolean sendData(Server s, ByteBuffer str, int client_id) {
                    str.put("Test".getBytes(Charset.forName("UTF-8")));
                    s.close(client_id);
                    return true;
                }
            });

            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
