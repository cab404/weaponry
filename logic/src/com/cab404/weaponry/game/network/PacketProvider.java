package com.cab404.weaponry.game.network;

import java.util.List;

/**
 * @author cab404
 */
public interface PacketProvider {
    public List<Packet> getPackets();
}
