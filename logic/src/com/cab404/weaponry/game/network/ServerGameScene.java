package com.cab404.weaponry.game.network;

import com.badlogic.gdx.graphics.Color;
import com.cab404.guidy.Luna;
import com.cab404.weaponry.game.network.server.*;
import com.cab404.weaponry.scene.PlatformerGameScene;
import com.cab404.weaponry.util.LOg;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author cab404
 */
public abstract class ServerGameScene extends PlatformerGameScene {

    ArrayList<ServerPacketProvider> providers;
    ArrayList<ServerPacketHandler> handlers;
    Map<Integer, CopyOnWriteArrayList<Packet>> packets;

    public void addPacketHandler(ServerPacketHandler handler) {
        handlers.add(handler);
    }

    public void removePacketHandler(ServerPacketHandler handler) {
        handlers.remove(handler);
    }

    public void addOutputListener(ServerPacketProvider provider) {
        providers.add(provider);
    }

    public void removeOutputListener(ServerPacketProvider provider) {
        providers.remove(provider);
    }


    public PlatformerServerConfig config;
    public Server server;
    {
        config = new PlatformerServerConfig();
        handlers = new ArrayList<>();
        providers = new ArrayList<>();
    }


    public ServerGameScene() {
        super();
    }

    @Override public void create() {
        super.create();

        providers = new ArrayList<>();
        packets = new HashMap<>();
        game = new SynchronizedStorage();

        providers.add(game.getPacketProvider());

        server = new Server(config.sconfig);
        bg.set(Color.BLACK);

        server.setInputListener(new PacketProcessorImpl());
        server.setOutputListener(new PacketSenderImpl());
        server.setConnectionGainListener(new NewConnectionListener());
        server.setConnectionLostListener(new DisconnectListener());

        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private class PacketSenderImpl implements OutputListener {

        @Override public boolean sendData(Server server, ByteBuffer str, int client_id) {
            if (packets.containsKey(client_id)) {
                CopyOnWriteArrayList<Packet> packs = packets.get(client_id);
                if (packs.size() > 0) {
                    packet_count++;
                    packs.remove(0).serialize(str);
                } else
                    return false;

                return true;
            } else {
                return false;
            }
        }

    }

    private class PacketProcessorImpl implements InputListener {

        @Override public void retrieveData(Server server, ByteBuffer buf, int client_id) {
            try {
                Packet packet = PacketTypeRegistry.getInstance().getPacketForData(buf);

                for (ServerPacketHandler handler : handlers) {
                    if (handler.handle(packet, client_id)) break;
                }

            } catch (Exception ex) {
                LOg.err("Error from client " + client_id);
                LOg.err(ex);
                LOg.err(ex.getStackTrace()[0]);

            }

        }
    }

    private class NewConnectionListener implements ConnectionListener {
        @Override public void process(Server server, InetAddress address, int client_id) {
            LOg.vbs(client_id + ": Connected.");
            packets.put(client_id, new CopyOnWriteArrayList<Packet>());
            onConnect(server, address, client_id);
        }
    }

    public abstract void onConnect(Server server, InetAddress address, int client_id);
    public abstract void onDisconnect(Server server, InetAddress address, int client_id);

    private class DisconnectListener implements ConnectionListener {

        @Override public void process(Server server, InetAddress address, int client_id) {
            packets.remove(client_id);
            onDisconnect(server, address, client_id);
            LOg.vbs(client_id + ": Disconnected.");
        }
    }

    public static class PlatformerServerConfig {
        ServerConfig sconfig;


        public PlatformerServerConfig() {
            sconfig = new ServerConfig();
            sconfig.packet_size = 40;
            sconfig.port = 65400;
        }


    }


    @SuppressWarnings("FieldCanBeLocal")
    private float
            packet_send_delay = 1f / 30f,
            packet_send_delay_gone = 0;


    int packet_count;
    float second;
    @Override public void update() {
        super.update();
        packet_send_delay_gone += Luna.dt();
        second += Luna.dt();
        if (second > 1) {
//            LOg.vbs(packet_count + " packets/second");
            packet_count = 0;
            second = 0;
        }

        if (packet_send_delay_gone > packet_send_delay) {
            packet_send_delay_gone = 0;
            for (ServerPacketProvider provider : providers) {
                List<Packet> broadcast = provider.getBroadcastPackets();
                synchronized (packets) {
                    for (Map.Entry<Integer, CopyOnWriteArrayList<Packet>> client : packets.entrySet()) {
                        List<Packet> personal = provider.getPackets(client.getKey());
                        client.getValue().addAll(personal);
                        client.getValue().addAll(broadcast);
                        personal.clear();
                    }
                }
                broadcast.clear();
            }
        }
    }


    @Override public void render() {
    }

    @Override public void dispose() {
        super.dispose();
        server.stop();
    }
}
