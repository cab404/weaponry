package com.cab404.weaponry.game.network.server;

/**
 * @author cab404
 */
public class ServerConfig {
    public int port = 65677;
    public int max_connections = 20;
    public int packet_size = 40;
    public boolean isDaemon = false;
    protected boolean isRunning = false;
}


