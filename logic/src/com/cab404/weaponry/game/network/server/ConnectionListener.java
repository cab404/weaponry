package com.cab404.weaponry.game.network.server;

import com.cab404.weaponry.game.network.server.Server;

import java.net.InetAddress;

public interface ConnectionListener {
    public void process(Server server, InetAddress address, int client_id);
}
