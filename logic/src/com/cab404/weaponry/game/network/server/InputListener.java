package com.cab404.weaponry.game.network.server;

import java.nio.ByteBuffer;

public interface InputListener {
    /**
     * Return false if you want to close the stream.
     */
    public void retrieveData(Server server, ByteBuffer buf, int client_id);
}
