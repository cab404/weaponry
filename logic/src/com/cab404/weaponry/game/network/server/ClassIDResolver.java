package com.cab404.weaponry.game.network.server;

import com.cab404.weaponry.game.GunGameObj;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cab404
 */
public class ClassIDResolver {
    private static ClassIDResolver singleton;

    private Map<Character, Class<? extends GunGameObj>> classes;

    public static ClassIDResolver getInstance() {
        if (singleton == null) {
            singleton = new ClassIDResolver();
        }
        return singleton;
    }

    private ClassIDResolver() {
        classes = new HashMap<>();
    }

    /**
     * Do not forget to add constructor without parameters! setState FTW!
     */
    public void register(char id, Class<? extends GunGameObj> clazz) {
        classes.put(id, clazz);
    }

    public char getID(Class<? extends GunGameObj> clazz) {
        for (Map.Entry<Character, Class<? extends GunGameObj>> e : classes.entrySet()) {
            if (e.getValue().equals(clazz)) {
                return e.getKey();
            }
        }
        throw new RuntimeException("Unregistred ID");
    }

    public GunGameObj getObject(char id) {
        if (classes.containsKey(id))
            try {
                return classes.get(id).getConstructor().newInstance();
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RuntimeException("Error while creating new instance!", e);
            }
        else throw new RuntimeException("No class found for id!");
    }

}
