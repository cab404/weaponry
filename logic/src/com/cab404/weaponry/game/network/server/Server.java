package com.cab404.weaponry.game.network.server;

import com.cab404.weaponry.util.LOg;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author cab404
 */
public class Server {
    private Server inner_this;
    protected ServerConfig vals;
    protected ServerSocket socket;
    protected InputListener inputListener;
    protected OutputListener outputListener;
    protected ConnectionListener connectionGainListener, connectionLostListener;
    protected CopyOnWriteArrayList<Linker> clients = new CopyOnWriteArrayList<>();


    public void setInputListener(InputListener inputListener) {
        this.inputListener = inputListener;
    }

    public void setOutputListener(OutputListener outputListener) {
        this.outputListener = outputListener;
    }

    public void setConnectionGainListener(ConnectionListener connectionGainListener) {
        this.connectionGainListener = connectionGainListener;
    }

    public void setConnectionLostListener(ConnectionListener connectionLostListener) {
        this.connectionLostListener = connectionLostListener;
    }


    private int threads = 0;
    private final Object thread_count_lock = new Object();

    public int getConnectionNum() {
        return clients.size();
    }

    public int getThreadNum() {
        return threads;
    }

    private void addT() {
        synchronized (thread_count_lock) {
            threads++;
        }
    }

    private void remT() {
        synchronized (thread_count_lock) {
            threads--;
        }
    }

    ConnectionListenerThread clt;

    public void stop() {
        vals.isRunning = false;
    }

    public void start()
    throws IOException {
        if (!vals.isRunning) {
            vals.isRunning = true;
            socket = new ServerSocket(vals.port);
            socket.setSoTimeout(500);
            clt = new ConnectionListenerThread();
            clt.setDaemon(vals.isDaemon);
            clt.start();
        }
    }

    public void close(int id) {
        clients.get(id).out.isDead = true;
    }

    public Server(ServerConfig params) {
        vals = params;
        inner_this = this;
    }

    private int inc = 0;
    private synchronized int getNewId() {
        return inc++;
    }


    private class ConnectionListenerThread extends Thread {

        public void run() {
            addT();
            threads++;
            while (vals.isRunning) {
                try {

                    for (int i = 0; i < clients.size(); i++) {
                        if (clients.get(i).isDead()) {
                            Linker removed = clients.remove(i);
                            removed.in.socket.close();
                            connectionLostListener.process(inner_this, removed.in.socket.getInetAddress(), removed.id);
                        }
                    }

                    if (clients.size() < vals.max_connections) {
                        final Socket accept;
                        accept = socket.accept();

                        new Thread() {
                            public void run() {
                                addT();
                                int id = getNewId();
                                clients.add(new Linker(id, accept));
                                connectionGainListener.process(inner_this, accept.getInetAddress(), id);
                                remT();
                            }
                        }.start();
                    }
                } catch (IOException e) {
                    if (!(e instanceof SocketTimeoutException))
                        LOg.dbg(e);
                }
            }
            if (clients.size() == 0)
                inc = 0;
            remT();
        }

    }

    private class InputListenerThread extends Thread {
        private Socket socket;
        boolean isDead = false;
        int id;

        private InputListenerThread(int id, Socket socket) {
            this.id = id;
            this.socket = socket;
        }

        public void run() {
            addT();
            try {
                InputStream str = new BufferedInputStream(socket.getInputStream());
                while (!socket.isClosed() && vals.isRunning && !isDead) {
                    ByteBuffer packet = ByteBuffer.allocate(vals.packet_size);
                    int read = str.read(packet.array());
                    if (read == -1) throw new EOFException();

                    if (!isDead)
                        inputListener.retrieveData(inner_this, packet, id);

                }
            } catch (Exception e) {
                LOg.dbg(e);

                isDead = true;
            }
            remT();
        }

    }

    private class OutputListenerThread extends Thread {
        private Socket socket;
        boolean isDead = false;
        int id;

        private OutputListenerThread(int id, Socket socket) {
            this.id = id;
            this.socket = socket;
        }

        public void run() {
            addT();
            try {
                OutputStream str = new BufferedOutputStream(socket.getOutputStream());
                while (!socket.isClosed() && vals.isRunning) {
                    ByteBuffer packet = ByteBuffer.allocate(vals.packet_size);

                    if (!isDead)
                        if (outputListener.sendData(inner_this, packet, id))
                            str.write(packet.array());

                    str.flush();
                }
            } catch (Exception e) {
                LOg.dbg(e);
                e.printStackTrace();

                isDead = true;
            }
            remT();
        }

    }

    private class Linker {
        int id;
        public OutputListenerThread out;
        public InputListenerThread in;

        private Linker(int id, Socket socket) {
            this.id = id;
            out = new OutputListenerThread(id, socket);
            in = new InputListenerThread(id, socket);

            out.setDaemon(true);
            in.setDaemon(true);

            out.start();
            in.start();
        }

        public boolean isDead() {
            return out.isDead || in.isDead;
        }

    }

}
