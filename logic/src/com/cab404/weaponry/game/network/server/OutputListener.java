package com.cab404.weaponry.game.network.server;

import com.cab404.weaponry.game.network.server.Server;

import java.nio.ByteBuffer;

public interface OutputListener {
    /**
     * Return false if you don't want to send anything.
     */
    public boolean sendData(Server server, ByteBuffer str, int client_id);
}
