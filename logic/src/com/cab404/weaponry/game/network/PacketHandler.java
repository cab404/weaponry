package com.cab404.weaponry.game.network;

/**
 * @author cab404
 */
public interface PacketHandler {
    public boolean handle(Packet packet);
}
