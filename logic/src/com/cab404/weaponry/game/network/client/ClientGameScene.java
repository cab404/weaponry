package com.cab404.weaponry.game.network.client;

import com.badlogic.gdx.graphics.Color;
import com.cab404.weaponry.game.network.SynchronizedStorage;
import com.cab404.weaponry.scene.PlatformerGameScene;

import java.io.IOException;

/**
 * @author cab404
 */
public class ClientGameScene extends PlatformerGameScene {


    public Client client;

    public ClientGameScene(String url, int port) {
        super();
        client = new Client();
        client.getConfig().address = url;
        client.getConfig().port = port;
    }

    @Override public void create() {
        super.create();

        game = new SynchronizedStorage();

        client.addPacketHandler(game.getPacketHandler());

        bg.set(Color.BLACK);


    }

    public void start() {
        try {
            client.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override public void update() {
        super.update();
        client.update();
    }

}
