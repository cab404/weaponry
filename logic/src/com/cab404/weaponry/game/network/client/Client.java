package com.cab404.weaponry.game.network.client;

import com.cab404.weaponry.game.network.Packet;
import com.cab404.weaponry.game.network.PacketHandler;
import com.cab404.weaponry.game.network.PacketProvider;
import com.cab404.weaponry.game.network.PacketTypeRegistry;
import com.cab404.weaponry.util.LOg;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author cab404
 */
public class Client {
    private ClientConfig config = new ClientConfig();
    private CopyOnWriteArrayList<Packet> in, out;
    private ArrayList<PacketHandler> handlers = new ArrayList<>();
    private ArrayList<PacketProvider> providers = new ArrayList<>();

    public ClientConfig getConfig() {
        return config;
    }

    public void addPacketHandler(PacketHandler handler) {
        handlers.add(handler);
    }

    public void removePacketHandler(PacketHandler handler) {
        handlers.remove(handler);
    }

    public void addPacketProvider(PacketProvider provider) {
        providers.add(provider);
    }

    public void removePacketProvider(PacketProvider provider) {
        providers.remove(provider);
    }

    public void sendPacket(Packet packet) {
        out.add(packet);
    }


    // Служебные классы
    public static class ClientConfig {
        public int packet_size = 40;
        public int port = 65400;
        public String address;
    }

    private class HandlerThread implements Runnable {

        private Socket socket;
        private boolean isDead = false;

        private HandlerThread(Socket socket) {
            this.socket = socket;
            in = new CopyOnWriteArrayList<>();
            out = new CopyOnWriteArrayList<>();
        }

        @Override public void run() {
            try {

                BufferedInputStream str = new BufferedInputStream(socket.getInputStream());

                while (!isDead) {
                    ByteBuffer buff = ByteBuffer.allocate(config.packet_size);
//                    int read = 0;
//                    while (read < (read +=
//                    ) && read < config.packet_size) {}
                    int read = 0;

                    while (read < config.packet_size) {
                        int tmp = str.read(buff.array(), read, config.packet_size - read);
                        if (tmp == -1) throw new RuntimeException("EOF");
                        read += tmp;
                    }

                    if (read != -1)
                        in.add(PacketTypeRegistry.getInstance().getPacketForData(buff));

                }

            } catch (IOException e) {
                LOg.err(e);
                throw new RuntimeException(e);
            }
        }
    }

    private class ProviderThread implements Runnable {
        Socket socket;
        private boolean isDead = false;

        private ProviderThread(Socket socket) {
            this.socket = socket;
        }

        @Override public void run() {
            try {
                BufferedOutputStream str = new BufferedOutputStream(socket.getOutputStream());

                while (!isDead) {
                    while (!out.isEmpty()) {
                        Packet packet = out.remove(0);
                        ByteBuffer buff = ByteBuffer.allocate(config.packet_size);
                        packet.serialize(buff);
                        str.write(buff.array());
                    }
                    str.flush();

                }


            } catch (IOException e) {
                LOg.err(e);
                throw new RuntimeException(e);
            }
        }
    }

    ProviderThread pt;
    HandlerThread ht;


    public void start()
    throws IOException {
        LOg.vbs(config.address);
        LOg.vbs(config.port);

        Socket socket = new Socket(config.address, config.port);

        pt = new ProviderThread(socket);
        ht = new HandlerThread(socket);

        new Thread(pt).start();
        new Thread(ht).start();

    }

    public void update() {
        for (Packet pack : in) {
            for (PacketHandler handler : handlers) {
                if (handler.handle(pack)) break;
            }
        }
        in.clear();

        for (PacketProvider provider : providers) {
            out.addAll(provider.getPackets());
        }
    }

    public void stop() {
        pt.isDead = ht.isDead = true;
    }

}
