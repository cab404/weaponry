package com.cab404.weaponry.game.network;

import com.cab404.defense.objects.GameObj;
import com.cab404.weaponry.game.GunGameObj;
import com.cab404.weaponry.game.network.packet.DelPacket;
import com.cab404.weaponry.game.network.packet.UpdPacket;
import com.cab404.weaponry.game.network.server.ClassIDResolver;
import com.cab404.weaponry.util.SimpleObjectStorage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Or Network Pony Storage
 *
 * @author cab404
 */

public class SynchronizedStorage extends SimpleObjectStorage {
    CopyOnWriteArrayList<Packet> in, out;
    boolean is_in, is_out;

    private int oIDG = Integer.MIN_VALUE;

    private synchronized int oIDGen() {
        return oIDG++;
    }


    public SynchronizedStorage() {
        super();
        in = new CopyOnWriteArrayList<>();
        out = new CopyOnWriteArrayList<>();
    }

    public class GetPacket implements ServerPacketProvider {
        @Override public List<Packet> getPackets(int id) {
            return generateCycle();
        }
        @Override public List<Packet> getBroadcastPackets() {
            return out;
        }
    }

    public ServerPacketProvider getPacketProvider() {
        is_out = true;
        return new GetPacket();
    }

    public PacketHandler getPacketHandler() {
        is_in = true;
        return new StoragePacketHandler();
    }

    @SuppressWarnings("ForLoopReplaceableByForEach")
    public GunGameObj getObjectByOID(int oid) {
        List<GameObj> all = (List<GameObj>) getAll();

        for (int i = 0; i < all.size(); i++) {
            GameObj iter = all.get(i);
            if (iter instanceof GunGameObj) {
                if (((GunGameObj) iter).oID == oid)
                    return (GunGameObj) iter;
            }
        }

        throw new RuntimeException("Object not found!");
    }

    public void remObjectByOID(int oid) {
        List<GameObj> all = (List<GameObj>) getAll();
        for (int i = 0; i < all.size(); i++) {
            GameObj iter = all.get(i);
            if (iter instanceof GunGameObj) {
                if (((GunGameObj) iter).oID == oid) {
                    all.remove(i);
                    return;
                }
            }
        }
//        throw new RuntimeException("Object not found!");
    }

    public class StoragePacketHandler implements PacketHandler {

        @Override public boolean handle(Packet packet) {
            in.add(packet);
            return false;
        }

    }

    @Override public void add(GameObj... add) {
        if (is_out)
            for (GameObj ta : add) {
                if (ta instanceof GunGameObj) {
                    ((GunGameObj) ta).oID = oIDGen();
                }
            }

        super.add(add);
    }

    @Override public void remove(GameObj... remove) {
        super.remove(remove);


        for (GameObj toRem : remove) {
            if (toRem instanceof GunGameObj) {

                GunGameObj ser = (GunGameObj) toRem;
                DelPacket packet = new DelPacket();

                packet.oID = ser.oID;

                out.add(packet);
            }
        }

    }

    public List<Packet> generateCycle() {
        ArrayList<Packet> out = new ArrayList<>();

        for (GameObj obj : getAll()) {
            if (obj instanceof GunGameObj) {

                GunGameObj ser = (GunGameObj) obj;
                UpdPacket packet = new UpdPacket();

                packet.hp = ser.hp;

                packet.x = ser.pos.x;
                packet.y = ser.pos.y;
                packet.vx = ser.vel.x;
                packet.vy = ser.vel.y;

                packet.oID = ser.oID;
                packet.sID = ser.getState();
                packet.cID = ClassIDResolver.getInstance().getID(ser.getClass());

                out.add(packet);
            }
        }
        return out;
    }

    @Override public void update(float time) {
        super.update(time);

        HashSet<Integer> ids = new HashSet<>();
        ArrayList<GameObj> rem = new ArrayList<>();
        for (GameObj obg : getAll()) {
            if (obg instanceof GunGameObj) {
                if (ids.contains(((GunGameObj) obg).oID)) {
                    rem.add(obg);
                } else {
                    ids.add(((GunGameObj) obg).oID);
                }
            }
        }
        remove(rem.toArray(new GameObj[rem.size()]));


        for (Packet packet : in) {
            if (packet instanceof UpdPacket) {
                UpdPacket pack = (UpdPacket) packet;
                GunGameObj obj;

                try {
                    obj = getObjectByOID(pack.oID);
                } catch (RuntimeException ex) {
                    obj = ClassIDResolver.getInstance().getObject(pack.cID);
                    obj.oID = pack.oID;
                    add(obj);
                }

                obj.vel.set(pack.vx, pack.vy);
                obj.pos.set(pack.x, pack.y);
                obj.setState(pack.sID);
                obj.hp = pack.hp;

            }
            if (packet instanceof DelPacket) {
                remObjectByOID(((DelPacket) packet).oID);
            }
        }

        in.clear();

    }
}
