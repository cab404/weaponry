package com.cab404.weaponry.util;

import com.cab404.defense.objects.GameObj;
import com.cab404.defense.storage.AbstractObjectStorage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * @author cab404
 */
public class SimpleObjectStorage extends AbstractObjectStorage {

    private ArrayList<GameObj> objects, add_buffer, rem_buffer;

    public SimpleObjectStorage() {
        super();
        objects = new ArrayList<>();
        add_buffer = new ArrayList<>();
        rem_buffer = new ArrayList<>();
    }

    @Override public void add(GameObj... add) {
        super.add(add);
        Collections.addAll(add_buffer, add);
    }

    @Override public void remove(GameObj... remove) {
        Collections.addAll(rem_buffer, remove);
    }

    @Override
    public Collection<GameObj> getAll() {
        return objects;
    }


    @Override public void update(float time) {
        super.update(time);

        objects.removeAll(rem_buffer);
        rem_buffer.clear();
        objects.addAll(add_buffer);
        add_buffer.clear();
    }
}
