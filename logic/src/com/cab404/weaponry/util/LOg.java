package com.cab404.weaponry.util;

/**
 * @author cab404
 */
public class LOg {
    public static final int
            ERROR = 0, SEVERE = 1, DEBUG = 2, VERBOSE = 3;

    private static int log_level = ERROR;

    public static void setLogLevel(int ll) {
        log_level = ll;
    }


    public static void err(Object obj) {
        if (log_level >= 0)
            System.out.println("ERR: " + obj);
    }

    public static void sev(Object obj) {
        if (log_level >= 1)
            System.out.println("SEV: " + obj);
    }

    public static void dbg(Object obj) {
        if (log_level >= 2)
            System.out.println("DBG: " + obj);
    }

    public static void vbs(Object obj) {
        if (log_level >= 3)
            System.out.println("VBS: " + obj);
    }
}
