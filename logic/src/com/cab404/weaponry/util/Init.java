package com.cab404.weaponry.util;

import com.cab404.weaponry.game.Player;
import com.cab404.weaponry.game.guns.GrenadeLauncher;
import com.cab404.weaponry.game.guns.HookShooter;
import com.cab404.weaponry.game.guns.Uzi;
import com.cab404.weaponry.game.network.PacketTypeRegistry;
import com.cab404.weaponry.game.network.packet.*;
import com.cab404.weaponry.game.network.server.ClassIDResolver;

/**
 * @author cab404
 */
public class Init {

    public static void run() {

        ClassIDResolver.getInstance().register((char) 0, Player.class);
        ClassIDResolver.getInstance().register((char) 1, GrenadeLauncher.Grenade.class);
        ClassIDResolver.getInstance().register((char) 2, Uzi.GunBullet.class);
        ClassIDResolver.getInstance().register((char) 3, HookShooter.Hook.class);

        PacketTypeRegistry.getInstance().register(AddPacket.class);
        PacketTypeRegistry.getInstance().register(UpdPacket.class);
        PacketTypeRegistry.getInstance().register(DelPacket.class);
        PacketTypeRegistry.getInstance().register(ActPacket.class);
        PacketTypeRegistry.getInstance().register(UseCharPacket.class);


    }

}
