package com.cab404.weaponry.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.GameScene;
import com.cab404.defense.ui.ListMenu;
import com.cab404.defense.ui.SelectableField;
import com.cab404.guidy.Luna;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.guidy.rpg.top_view.TiledMapWrapper;
import com.cab404.guidy.tests.platformer.PlatformerObject;
import com.cab404.weaponry.game.GunGameObj;
import com.cab404.weaponry.game.assist.ImageObj;
import com.cab404.weaponry.game.assist.U;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cab404
 */
public class Menu extends GameScene {
    private List<BoundingObject> bb = new ArrayList<>();
    private ListMenu menu;
    private TiledMapWrapper map;


    private float w, h;

    @Override public void create() {

        Gdx.input.setInputProcessor(new U.PipeInputProcessor());
        Texture.setEnforcePotImages(false);

        super.create();

//      Добавляем логотип
        objects.gfx.cam.zoom = 0.7f;
        objects.add(
                new ImageObj(
                        loadSprite("data/imgs/title.png")
                )
                        .setPos(new Vector2(30, 30))
                        .setSize(new Vector2(31 * 4, 15 * 4)
                        )
        );

//      Добавляем меню.
        menu = new ListMenu();
        menu.animationSpeed = Float.MIN_VALUE;
        menu.add(new SelectableField() {
            boolean sel = false;

            @Override public void onSelect(Vector2 mouse) {
                if (sel) return;
                sel = true;
                Gdx.input.getTextInput(new Input.TextInputListener() {
                    @Override public void input(String text) {
                        sel = false;

                        Luna.scene.changeScene(
                                new Game(text.split(":")[0],
                                        text.split(":").length < 2 ? 65400
                                                :
                                                Integer.parseInt(text.split(":")[1])
                                )
                        );
                    }
                    @Override public void canceled() {
                        sel = false;
                    }
//
                }, "Адрес сервера", "cabinet404.ru:65400");
//
            }
            @Override public String getText() {
                return "Play!";
            }
        }
        );
        objects.add(menu);


//      Загружаем карту.
        map = new
                TiledMapWrapper("data/maps/tf_map", 32);
        map.setBoundingObjects(bb);

        game.add(map);
        bb.add(map);


        //      Создаём поня.
        PlatformerObject obj = new GunGameObj() {
            Sprite spr = loadSprite("data/imgs/chars/cab.png");
            {size = new Vector2(64, 64);}
            @Override public void render(SpriteBatch batch) {
                spr.setSize(64, 64);
                spr.setPosition(pos.x, pos.y);
                spr.draw(batch);
            }

            @Override public void setState(int state) {

            }
            @Override public void update(float time) {
                spr.setRegion(0, 0, 16, 16);
                spr.flip(!spr.isFlipX(), false);
                super.update(time);
                if (isOnGround())
                    vel.add(0, 350);
            }
        };
        obj.setBoundingObjects(bb);
        obj.pos.set(1100, 2000);
        game.add(obj);
        bb.add(obj);


        //      Фокусируем камеру на поне.
        Luna.CameraLinkParams clp = new Luna.CameraLinkParams();
        clp.target = obj;
        clp.zoom = 0.8f * (1 / Gdx.graphics.getDensity());
        clp.speed = 0.5f;
        clp.bounds = map.getBounds();
        game.add(Luna.getCameraLink(clp));


    }

    @Override public void update() {
        super.update();

        game.gfx.cam.translate(Luna.mouse(0).sub(w / 2, h / 2).div(2));

        menu.pos.set(w - menu.size.x - 20, (h - menu.size.y * 2));

    }

    @Override public void resize(Vector2 size) {
        super.resize(size);
        objects.gfx.update();

        OrthographicCamera cam = objects.gfx.cam;
        w = cam.viewportWidth * cam.zoom;
        h = cam.viewportHeight * cam.zoom;


    }

}
