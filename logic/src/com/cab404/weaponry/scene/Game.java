package com.cab404.weaponry.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.cab404.defense.objects.GameObj;
import com.cab404.guidy.Luna;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.guidy.rpg.top_view.TiledMapWrapper;
import com.cab404.weaponry.game.MainPlayerController;
import com.cab404.weaponry.game.Player;
import com.cab404.weaponry.game.network.Packet;
import com.cab404.weaponry.game.network.PacketHandler;
import com.cab404.weaponry.game.network.client.ClientGameScene;
import com.cab404.weaponry.game.network.packet.ActPacket;
import com.cab404.weaponry.game.network.packet.UseCharPacket;
import com.cab404.weaponry.util.LOg;

import java.util.ArrayList;

/**
 * @author cab404
 */
public class Game extends ClientGameScene {

    private int seekeng = 0;
    ArrayList<BoundingObject> bb = new ArrayList<>();
    Luna.CameraLinkParams params;
    Rectangle bounds;
    TiledMapWrapper map;
    Color bg = Color.valueOf("091129");

    public Game(String url, int port) {
        super(url, port);

    }


    private void add(BoundingObject obj) {
        game.add(obj);
        bb.add(obj);
        obj.setBoundingObjects(bb);
    }

    private void add(GameObj obj) {
        game.add(obj);
    }

    @Override public void create() {
        super.create();

        LOg.err(Color.WHITE.toString());

        map = new TiledMapWrapper("data/maps/tf_map", 32);
        add(map);

        map.isSolidOutside = false;

        client.addPacketHandler(new PacketHandler() {
            @Override public boolean handle(Packet packet) {
                if (packet instanceof UseCharPacket) {
                    seekeng = ((UseCharPacket) packet).oID;
                    return true;
                }
                return false;
            }
        });
//
//        Gdx.input.setInputProcessor(new GameScene.ZoomInputProcessor(game.gfx));

//        Player player = new Player(loadSprite("data/imgs/chars/cab.png"));
//        add(player);
//
//        player.weaponry.list.add(new GrenadeLauncher());
//        player.weaponry.list.add(new Uzi());
//
//        player.pos.set(2000, 2000);


        bounds = map.getBounds();
        bounds.x -= 6000;
        bounds.y -= 6000;
        bounds.width += 6000 * 2;
        bounds.height += 6000 * 2;

        params = new Luna.CameraLinkParams();
        params.target = map;
        params.zoom = 0.2f;
        params.bounds = bounds;

        add(Luna.getCameraLink(params));


//        player.setController(new MainPlayerController(player));

        start();
    }

    @Override public void update() {
        Gdx.gl.glClearColor(bg.r, bg.g, bg.b, bg.a);

        try {
            params.target = game.getObjectByOID(seekeng);
            params.zoom = 1;
        } catch (Exception ex) {
            params.target = map;
            params.zoom = 0.2f;
        }

        super.update();

//        game.gfx.cam.translate(Luna.mouse(0).sub(Luna.screen().div(2)).div(20));
//
//        if (Luna.key(Input.Keys.LEFT))
//            game.gfx.cam.translate(-20, 0);
//
//        if (Luna.key(Input.Keys.RIGHT))
//            game.gfx.cam.translate(20, 0);
//
//        if (Luna.key(Input.Keys.UP))
//            game.gfx.cam.translate(0, 20);
//
//        if (Luna.key(Input.Keys.DOWN))
//            game.gfx.cam.translate(0, -20);

        if (params.target == null || !params.target.isAlive) {
            params = new Luna.CameraLinkParams();
            params.target = map;
            params.zoom = 0.2f;
            params.bounds = bounds;
        }

        if (Luna.key(Input.Keys.W))
            client.sendPacket(new ActPacket(0));
        if (Luna.key(Input.Keys.S))
            client.sendPacket(new ActPacket(1));
        if (Luna.key(Input.Keys.A))
            client.sendPacket(new ActPacket(2));
        if (Luna.key(Input.Keys.D))
            client.sendPacket(new ActPacket(3));
        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT))
            client.sendPacket(new ActPacket(4));
        if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT))
            client.sendPacket(new ActPacket(5));

        for (GameObj obj : game.getAll()) {
            if (obj instanceof BoundingObject) {((BoundingObject) obj).setBoundingObjects(bb);}
            if (obj instanceof Player) {

                if (!((Player) obj).hasController())
                    ((Player) obj).setController(new MainPlayerController());
            }
        }


    }


}
