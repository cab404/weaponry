package com.cab404.weaponry.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import com.cab404.defense.scene.GameScene;
import com.cab404.guidy.Luna;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.guidy.rpg.top_view.TiledMapWrapper;
import com.cab404.weaponry.game.GunGameObj;
import com.cab404.weaponry.game.Player;
import com.cab404.weaponry.game.assist.U;
import com.cab404.weaponry.game.guns.Uzi;
import com.cab404.weaponry.game.network.Packet;
import com.cab404.weaponry.game.network.ServerGameScene;
import com.cab404.weaponry.game.network.ServerPacketHandler;
import com.cab404.weaponry.game.network.ServerPacketProvider;
import com.cab404.weaponry.game.network.packet.ActPacket;
import com.cab404.weaponry.game.network.packet.UseCharPacket;
import com.cab404.weaponry.game.network.server.Server;
import com.cab404.weaponry.util.LOg;
import com.cab404.weaponry.util.SL;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author cab404
 */
public class ServerScene extends ServerGameScene {

    List<BoundingObject> bb = new CopyOnWriteArrayList<>();
    HashMap<Integer, Gamer> players;
    TiledMapWrapper map;
    Rectangle bounds;
    Color bg = Color.valueOf("091129");


    private void add(BoundingObject obj) {
        game.add(obj);
        bb.add(obj);
        obj.setBoundingObjects(bb);
    }

    private void add(GameObj obj) {
        game.add(obj);
    }

    @Override public void create() {
        super.create();
        SL.inst().load("data/imgs/chars/cab.png");
        SL.inst().load("data/imgs/weapons/bomb.png");
        SL.inst().load("data/imgs/weapons/bullet.png");
        SL.inst().load("data/imgs/weapons/rope.png");

        map = new TiledMapWrapper("data/maps/tf_map", 32);
        add(map);

        map.isSolidOutside = false;
        players = new HashMap<>();

        bounds = map.getBounds();
        bounds.x -= 6000;
        bounds.y -= 6000;
        bounds.width += 6000 * 2;
        bounds.height += 6000 * 2;

        Gdx.input.setInputProcessor(new GameScene.ZoomInputProcessor(game.gfx));

    }

    @Override public synchronized void onConnect(Server server, InetAddress address, int client_id) {
        players.put(client_id, new Gamer(client_id));
    }

    @Override public synchronized void onDisconnect(Server server, InetAddress address, int client_id) {
        Gamer gamer = players.remove(client_id);
        if (gamer == null) return;
        gamer.remove();

    }

    private class Gamer {
        int id;
        Player associated;
        ArrayList<Packet> packets;
        ServerPacketHandler control;
        ServerPacketProvider provider;

        public boolean up, down, left, right;
        public boolean lm, rm;
        public Vector2 mouse_pos;


        public Gamer(int id) {
            packets = new ArrayList<>();
            this.id = id;

            associated = new Player();
            associated.weaponry.list.add(new Uzi());
            associated.pos.set(2000, 2000);

            control = new Control();
            provider = new Sender();

            add(associated);

            packets.add(new UseCharPacket(associated.oID));
            LOg.vbs(associated.oID);

            addPacketHandler(control);
            addOutputListener(provider);
        }

        public void remove() {
            removePacketHandler(control);
            removeOutputListener(provider);
            game.remove(associated);
            bb.remove(associated);
        }

        class Sender implements ServerPacketProvider {
            @Override public List<Packet> getPackets(int client_id) {
                if (client_id == id)
                    return packets;
                return new ArrayList<>();
            }
            @Override public List<Packet> getBroadcastPackets() {
                return new ArrayList<>();
            }
        }

        public void reset() {
            up = down = left = right = lm = rm = false;
            mouse_pos = null;
        }

        class Control implements ServerPacketHandler {

            @Override public boolean handle(Packet packet, int cid) {
                if (id != cid) return false;
                if (packet instanceof ActPacket) {
                    mouse_pos = ((ActPacket) packet).mouse;
                    switch (((ActPacket) packet).aID) {
                        case 0:
                            up = true;
                            break;
                        case 1:
                            down = true;
                            break;
                        case 2:
                            left = true;
                            break;
                        case 3:
                            right = true;
                            break;
                        case 4:
                            lm = true;
                            break;
                        case 5:
                            rm = true;
                            break;
                    }
                    return true;
                }
                return false;
            }
        }

    }

    @Override public void update() {
        Gdx.gl.glClearColor(bg.r, bg.g, bg.b, bg.a);
        super.update();

//        game.gfx.cam.translate(Luna.mouse(0).sub(Luna.screen().div(2)).div(20));

        float time = Luna.dt() * time_scale;
        for (Gamer gamer : players.values()) {
            Player obj = gamer.associated;

            if (gamer.left) {
                if (obj.isOnGround())
                    obj.vel.x -= 1000 * time;
                else
                    obj.vel.x -= 400 * time;
            }

            if (gamer.right) {
                if (obj.isOnGround())
                    obj.vel.x += 1000 * time;
                else
                    obj.vel.x += 400 * time;
            }

            if (gamer.up && obj.isOnGround()) {
                obj.vel.y = 700;
            }
            if (gamer.down) {
                obj.vel.y -= 3000 * time;
            }

            if (obj.weaponry.get() != null)
                obj.weaponry.get().update(time);

            if (gamer.mouse_pos != null)
                obj.isFlipped = gamer.mouse_pos.x > 0;
            if (gamer.lm) {
                ArrayList<GunGameObj> bullets = new ArrayList<>();
                if (obj.weaponry.get() != null)
                    obj.weaponry.get().shoot(obj, bullets, gamer.mouse_pos.nor());
                Collections.addAll(U.aTGame, bullets.toArray(new GunGameObj[bullets.size()]));
            }

            gamer.reset();
        }

        for (GameObj obj : game.getAll()) {
            obj.isAlive = obj.isAlive && obj.getBounds().overlaps(bounds);
        }


    }

}
