package com.cab404.weaponry.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL11;
import com.cab404.defense.objects.GameObj;
import com.cab404.defense.scene.Scene;
import com.cab404.weaponry.game.assist.U;
import com.cab404.weaponry.game.network.SynchronizedStorage;
import com.cab404.weaponry.util.SimpleObjectStorage;

/**
 * @author cab404
 */
public class PlatformerGameScene extends Scene {
    public SynchronizedStorage game;
    public SimpleObjectStorage particles;
    public Color bg;

    public int iterations = 1;
    public float time_scale = 1f;


    @Override public void create() {
        particles = new SimpleObjectStorage();
        objects = new SimpleObjectStorage();
        game = new SynchronizedStorage();

        bg = Color.WHITE.cpy();

        particles.gfx.cam = game.gfx.cam;
    }

    @Override public void update() {
        particles.add(U.aTPartc.toArray(new GameObj[U.aTPartc.size()]));
        objects.add(U.aTObj.toArray(new GameObj[U.aTObj.size()]));
        game.add(U.aTGame.toArray(new GameObj[U.aTGame.size()]));

        U.aTPartc.clear();
        U.aTGame.clear();
        U.aTObj.clear();

        objects.update(iterations, Gdx.graphics.getDeltaTime());
        particles.update(iterations, Gdx.graphics.getDeltaTime() * time_scale);
        game.update(iterations, Gdx.graphics.getDeltaTime() * time_scale);
    }


    public void render() {
        Gdx.gl.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glClearColor(bg.r, bg.g, bg.b, 1);

        particles.render();
        game.render();
        objects.render();

    }
}
